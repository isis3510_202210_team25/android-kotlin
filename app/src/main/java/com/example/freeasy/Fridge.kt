package com.example.freeasy

import android.content.Intent
import android.media.Image
import android.os.Bundle
import android.os.PersistableBundle
import android.view.LayoutInflater
import android.widget.Button
import android.widget.ListView
import java.util.*
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.freeasy.databinding.ActivityFridgeBinding
import com.example.freeasy.databinding.ActivityMainBinding
import kotlin.collections.ArrayList

class Fridge : AppCompatActivity() {
    // initiate viewBinding
    var arrItem: ArrayList<FridgeItem> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_fridge)
        val pName = intent.getStringExtra("name")
        val quantity = intent.getStringExtra("quantity").toString()
        val image = intent.getIntExtra("image", 0)
        val n : String  = pName.toString()
        val q: String = quantity.toString()
        arrItem.add(FridgeItem(q, n, image))
        var listView = findViewById(R.id.listView) as ListView
        listView.adapter = CustomAdapter(applicationContext, arrItem)



        val addButton = findViewById<Button>(R.id.addButton)
        addButton.setOnClickListener {
            val intent = Intent(this, AddItem::class.java)
            startActivity(intent)
        }
    }

}