package com.example.freeasy

import java.util.*

data class FridgeItem(var quantity:String, var name: String, var image: Int)
