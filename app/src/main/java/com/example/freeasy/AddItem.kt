package com.example.freeasy

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.Spinner

class AddItem : AppCompatActivity() {
    lateinit var pName: EditText
    lateinit var quantity: EditText
    var image: Int = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_item)
        pName = findViewById(R.id.itemSpinner)
        quantity = findViewById(R.id.txtQuantity)
        var s: String = pName.text.toString()
        var q: String = quantity.text.toString()
        if (s == "tomato"){
            image= R.drawable.tomato
        } else if (s == "carrot"){
            image = R.drawable.carrot
        } else if(s == "onion"){
            image = R.drawable.onion
        } else if(s == "lettuce"){
            image = R.drawable.lettuce
        } else{
            image = R.drawable.lentils
        }
        val b = findViewById<Button>(R.id.backButton)
        b.setOnClickListener {
            val intent = Intent(this, Fridge::class.java)
            startActivity(intent)
        }
        val submitButton = findViewById<Button>(R.id.submitButton)
        submitButton.setOnClickListener {
            val intent = Intent(this, Fridge::class.java)
            intent.putExtra("name", s)
            intent.putExtra("quantity", q)
            intent.putExtra("image", image)
            startActivity(intent)
        }
    }
}