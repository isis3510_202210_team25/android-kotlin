package com.example.freeasy

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val groceriesActButton = findViewById<Button>(R.id.buttonStorage)
        groceriesActButton.setOnClickListener {
            val intent = Intent(this, Storage::class.java)
            startActivity(intent)
        }
    }
}