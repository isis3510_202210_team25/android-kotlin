package com.example.freeasy

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import java.util.ArrayList

class CustomAdapter(var context: Context, var item: ArrayList<FridgeItem>): BaseAdapter() {

    private class ViewHolder(row: View?){
        var txtName: TextView
        var ivImage: ImageView
        var txtQuantity: TextView

        init {
            this.txtName = row?.findViewById(R.id.txtItem) as TextView
            this.ivImage = row?.findViewById(R.id.ivItem) as ImageView
            this.txtQuantity = row?.findViewById(R.id.txtQuantity) as TextView
        }
    }

    override fun getCount(): Int {
        return item.count()
    }

    override fun getItem(p0: Int): Any {
        return item.get(p0)
    }

    override fun getItemId(p0: Int): Long {
        return p0.toLong()
    }

    override fun getView(p0: Int, p1: View?, p2: ViewGroup?): View {
        var view: View?
        var viewHolder: ViewHolder
        if(p1 == null){
            var layout = LayoutInflater.from(context)
            view = layout.inflate(R.layout.fridge_item_list, p2, false)
            viewHolder = ViewHolder(view)
            view.tag = viewHolder
        } else {
            view = p2
            viewHolder = view?.tag as ViewHolder
        }
        var item: FridgeItem = getItem(p0) as FridgeItem
        viewHolder.txtName.text = item.name
        viewHolder.ivImage.setImageResource(item.image)
        viewHolder.txtQuantity.text = item.quantity

        return view as View
    }
}