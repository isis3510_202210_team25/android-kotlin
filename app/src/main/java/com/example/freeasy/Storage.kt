package com.example.freeasy

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button

class Storage : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_storage)

        val groceriesActButton = findViewById<Button>(R.id.buttonFridge)
        groceriesActButton.setOnClickListener {
            val Intent = Intent(this, Fridge::class.java)
            startActivity(Intent)
        }
    }
}